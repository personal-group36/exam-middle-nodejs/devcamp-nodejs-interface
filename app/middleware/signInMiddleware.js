const logCurrentTimeMiddleware = function(request, response, next) {
    var currentDate = new Date(); 
    var currentTime = "Time: "
                    + currentDate.getHours() + "h"  
                    + currentDate.getMinutes();
    console.log(currentTime);
    next();
}
var url = require('url');

const logRequestUrlMiddleware = function(request, response, next) {
    console.log("URL: ",url.format({
        protocol: request.protocol,
        host: request.get('host'),
        pathname: request.originalUrl
    }))
    next();
}
module.exports = {
    logCurrentTimeMiddleware,
    logRequestUrlMiddleware
}