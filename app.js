// khai báo thư viện cần dùng
const express = require("express");
const app = express();

// khai báo middleware
const signInMiddleware = require("./app/middleware/signInMiddleware")

app.use(signInMiddleware.logCurrentTimeMiddleware, signInMiddleware.logRequestUrlMiddleware)

// khi GET trả về trang chủ
app.use(express.static(__dirname + "/views"));
app.get("/", function(request, response) {
    response.sendFile(__dirname + "/views/sign-in.html")
})

// khai báo cổng
const port = 8000;
app.listen(port, function() {
    console.log("App running on port 8000");
})